BASE_IMAGE_REPOSITORY=docker.io/php
DOCKER_REGISTRY=registry.gitlab.com
DOCKER_IMAGE_NAME=php
DOCKER_REPOSITORY_BASEPATH=cloudmigration/build-images
PHP_VERSIONS=7.2-buster
PHP_VERSIONS += 7.3-buster
PHP_VERSIONS += 7.4-buster
PHP_VERSIONS += 7.2-alpine3.9 7.2-alpine3.10
PHP_VERSIONS += 7.3-alpine3.9 7.3-alpine3.10
PHP_VERSIONS += 7.4-alpine3.10
BASE_IMAGE_VERSIONS=$(PHP_VERSIONS)
