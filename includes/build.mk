ifdef DOCKER_BASE_IMAGE_TAG
_docker-build:
	$(DOCKER_BUILD)\
		--build-arg GCC_PKG_NAME=$(GCC_PKG_NAME)\
		--build-arg GPP_PKG_NAME=$(GPP_PKG_NAME)\
		-f Dockerfile.build.$(OS_NAME)
endif

